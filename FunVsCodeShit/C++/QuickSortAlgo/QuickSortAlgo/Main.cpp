#include <iostream>
#include <vector>

void BubbleSortAlgo(std::vector<int>& aVect) {
    std::vector<int>::iterator iter;

    for (iter = aVect.begin(); iter < aVect.end(); iter++) {
        if (iter != aVect.end() - 1 && *iter > *(iter++))
        {
            int temp = *iter;
            *iter = *iter++;
            *iter++ = temp;
        }
    }
}

int main() {

    std::vector<int> vector = { 6,9,3,5,1,8,0,7,2 };
    std::vector<int>::iterator iter;

    for (iter = vector.begin(); iter < vector.end(); iter++) {
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    BubbleSortAlgo(vector);

    for (iter = vector.begin(); iter < vector.end(); iter++) {
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    system("pause");
    return 0;
}