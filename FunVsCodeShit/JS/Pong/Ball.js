class Ball {
    constructor(speed = 5) {
        this.x = width * 0.5;
        this.y = height * 0.5;
        this.dia = 20;
        this.speed = speed;
        this.velX = this.speed;
        this.velY = random(-this.speed, this.speed);

        let rand = round(random(1));
        if (rand == 1) {
            this.velX = -this.velX;
        }
    }

    collide(paddle) {
        this.hit = collideRectCircle(paddle.x, paddle.y, paddle.w, paddle.h, this.x, this.y, this.dia);
        if (this.hit) {
            this.velX = -this.velX; // this needs to add velY of paddle to ball and send ball back
            this.velY = paddle.velY;
        }
    }

    move() {
        if (this.y <= 0) {
            this.velY = -this.velY;
        }

        if (this.y > height) {
            this.velY = -this.velY;
        }

        this.x += this.velX;
        this.y += this.velY;
    }

    draw() {
        fill(255);
        noStroke();
        ellipse(this.x, this.y, this.dia);
    }
}