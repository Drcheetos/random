// Game Variables
let ball;
let paddleR;
let rightControls = [38,40];
let paddleL;
let leftControls = [87,83];
let bot;
let temp;
let botIsPlaying = true;
let gameOver = false;
let winner = 0;
let gameSpeedModif = 1.15;
let tempSpeed;

// MainMenu Variables
const menuTxt = "This game was made for Coco.\nFirst player to reach 10 points wins.\nLeft controls : W = up, S = down\nRight controls : up arrow, down arrow\nHave Fun !\n\np.s. if you play one player,\nyou are the left paddle using the left controls.";
let onMainMenu = true;
let hasChosen = false;
let onePlayerButton;
let twoPlayerButton;
let returnButton;
let resetBallButton;

function setup() {
    createCanvas(600, 600);
    textFont('Helvetica');
    createUIButtons();
    
    ball = new Ball();
    bot = new AI(width - 40);
    paddleL = new Paddle(20, leftControls);
    temp = new Paddle(width - 40, rightControls);
}

function draw() {
    background(0);

    if (onMainMenu) {
        fill(255);
        textSize(20);
        textAlign(CENTER, CENTER);
        text(menuTxt, width * 0.5, height * 0.25);
        
        onePlayerButton.draw();
        twoPlayerButton.draw();
        return;
    }
    
    if (gameOver) {
        fill(255);
        textSize(20);
        textAlign(CENTER, CENTER);
        
        if (winner == 1) {
            text("Player one wins !", width * 0.5, height * 0.5);
            returnButton.draw();
        }
        else if (winner == 2) {
            text("Player two wins !", width * 0.5, height * 0.5);
            returnButton.draw();
        }
        else if (winner == 3) {
            text("Bot wins !", width * 0.5, height * 0.5);
            returnButton.draw();
        }
        
        return;
    }

    if (hasChosen && !botIsPlaying) {
        paddleR = temp;
        hasChosen = false;
    }
    else if (hasChosen && botIsPlaying) {
        paddleR = bot;
        hasChosen = false;
    }

    drawMidLine();

    ball.draw();
    checkBallX();

    paddleL.draw();
    if (!botIsPlaying) {
        paddleR.draw();
    }
    else {
        bot.draw(ball);
    }

    ball.collide(paddleL);
    ball.collide(paddleR);

    ball.move();

    textSize(40);
    text(paddleL.score, width * 0.35, height * 0.15);
    text(paddleR.score, width * 0.63, height * 0.15);

    resetBallButton.draw();
}

function drawMidLine() {
    for (let i = 0; i < 60; i++) {
        if (i % 2) {
            fill(255);
        } else {
            fill(0);
        }

        rect(width * 0.5, 0 + (i * 10) - 5, 10, 10);
    }
}

function checkBallX() {
    if (ball.x > width) {
        paddleL.score += 1;
        console.log("Player 1 : " + paddleL.score);
        if (paddleL.score == 10) {
            gameOver = true;
            winner = 1;
        }
        spawnNewBall();
    }
    else if (ball.x < 0) {
        paddleR.score += 1;
        console.log("Player 2 : " + paddleR.score);
        if (paddleR.score == 10 && !botIsPlaying) {
            gameOver = true;
            winner = 2;
        }
        else if (paddleR.score == 10 && botIsPlaying) {
            gameOver = true;
            winner = 3;
        }
        spawnNewBall();
    }
}

function spawnNewBall() {
    tempSpeed = ball.speed;
    tempSpeed *= gameSpeedModif;
    if (tempSpeed > 10) {
        tempSpeed = 10;
    }
    ball = new Ball(tempSpeed);

    paddleL.speed *= gameSpeedModif;
    if (paddleL.speed > 10) {
        paddleL.speed = 10;
    }

    paddleR.speed *= gameSpeedModif;
    if (paddleR.speed > 10) {
        paddleR.speed = 10;
    }
    console.log(ball.speed);
}

function createUIButtons() {
    onePlayerButton = new Clickable();
    onePlayerButton.cornerRadius = 45;
    onePlayerButton.locate(width * 0.37, height * 0.50);
    onePlayerButton.resize(width * 0.25, height * 0.05);
    onePlayerButton.onOutside = function(){
        this.strokeWeight = 0;
        this.color = "#FFFFFF";
        this.text = "One Player";
    }
    onePlayerButton.onHover = function(){
        this.color = "#808080";
    }
    onePlayerButton.onPress = function(){
        this.color = "#4d4d4d";
        botIsPlaying = true;
        hasChosen = true;
        onMainMenu = false;
    }
    
    twoPlayerButton = new Clickable();
    twoPlayerButton.cornerRadius = 45;
    twoPlayerButton.locate(width * 0.37, height * 0.60);
    twoPlayerButton.resize(width * 0.25, height * 0.05);
    twoPlayerButton.onOutside = function(){
        this.strokeWeight = 0;
        this.color = "#FFFFFF";
        this.text = "Two Players";
    }
    twoPlayerButton.onHover = function(){
        this.color = "#808080";
    }
    twoPlayerButton.onPress = function(){
        this.color = "#4d4d4d";
        botIsPlaying = false;
        hasChosen = true;
        onMainMenu = false;
    }
    
    returnButton = new Clickable();
    returnButton.cornerRadius = 45;
    returnButton.locate(width * 0.37, height * 0.60);
    returnButton.resize(width * 0.25, height * 0.05);
    returnButton.onOutside = function(){
        this.strokeWeight = 0;
        this.color = "#FFFFFF";
        this.text = "Return";
    }
    returnButton.onHover = function(){
        this.color = "#808080";
    }
    returnButton.onPress = function(){
        this.color = "#4d4d4d";
        hasChosen = false;
        onMainMenu = true;
        gameOver = false;
        winner = 0;
        ball = new Ball();
        bot = new AI(width - 40);
        paddleL = new Paddle(20, leftControls);
        temp = new Paddle(width - 40, rightControls);
    }

    resetBallButton = new Clickable();
    resetBallButton.cornerRadius = 45;
    resetBallButton.locate(width * 0.55, height * 0.92);
    resetBallButton.resize(width * 0.05, height * 0.05);
    resetBallButton.onOutside = function(){
        this.strokeWeight = 0;
        this.color = color(255, 100);
        this.text = "";
    }
    resetBallButton.onHover = function(){
        this.color = color(128, 128, 128, 100);
    }
    resetBallButton.onPress = function(){
        this.color = color(77, 77, 77, 100);
        ball = new Ball(tempSpeed);
    }
}