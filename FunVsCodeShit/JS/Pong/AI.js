class AI {
    constructor(x) {
        this.x = x;
        this.y = height * 0.5 - 75; // Will become controllable eventually
        this.h = 150;
        this.w = 20;
        this.score = 0;
        this.velY = 0;
        this.speed = 5;
    }

    move(ball) {
        // Make paddle move up down Y axis
        if (this.y + this.h > ball.y && this.y < ball.y ) {
            this.velY = 0;
        }
        else if (this.y + this.h - ball.dia > ball.y && this.y > 0) {
            this.velY = -this.speed;
        }
        else if (this.y + ball.dia < ball.y && this.y + this.h <= height) {
            this.velY = this.speed;
        }
        else {
            this.velY = 0;
        }

        this.y += this.velY;
    }

    draw(ball) {
        fill(255);
        rect(this.x, this.y, this.w, this.h);
        this.move(ball);
    }
}