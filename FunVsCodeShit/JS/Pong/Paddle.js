class Paddle {
    constructor(x, controls) {
        this.x = x;
        this.y = height * 0.5 - 75;
        this.h = 150;
        this.w = 20;
        this.score = 0;
        this.velY = 0;
        this.speed = 5;
        this.controls = controls;
    }

    move() {
        if (keyIsDown(this.controls[0]) && this.y > 0) {
            this.velY = -this.speed;
        }
        else if (keyIsDown(this.controls[1]) && this.y + this.h <= height) {
            this.velY = this.speed;
        }
        else {
            this.velY = 0;
        }

        this.y += this.velY;
    }

    draw() {
        fill(255);
        rect(this.x, this.y, this.w, this.h);

        this.move();
    }
}