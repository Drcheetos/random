let players = [];
let walls = [];
let numberOfWalls = 5;
let numberOfPlayers = 100;
let floorY;

function setup() {
  createCanvas(1000, 600);
  floorY = height * 0.80;
  
  for (let i = 0; i < numberOfPlayers; i++) {
    players.push(new Player(floorY));
  }

  for (let i = 0; i < numberOfWalls; i++) {
    walls.push(new Wall(random() * width, random(100,floorY)));
  }
}

function draw() {
  background(0);
  
  // Floor
  stroke(255);
  line(0, floorY, width, floorY);
  
  // Target
  noStroke();
  fill(0, 255, 0);
  ellipse(width - 50, floorY - 50, 50);

  for (let i = 0; i < walls.length; i++) {
    walls[i].show();
  }
  
  for (let i = 0; i < players.length; i++) {
    players[i].show();
    for (let j = 0; j < walls.length; j++) {
      players[i].collision(walls[j]);
    }
  }
}
