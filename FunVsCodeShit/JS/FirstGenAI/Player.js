class Player {
    constructor(aY) {
        this.w = 25;
        this.x = this.w * 0.25;
        this.h = this.w;
        this.y = aY - this.h;
        this.numOfMoves = 30;
        this.moves = [];
        this.pos = createVector(this.x, this.y);
        this.vel = createVector();
        this.acc = createVector();
        this.time = 0;
        this.dead = false;

        for (let i = 0; i < this.numOfMoves; i++) {
            let randomForce = createVector(random(0, 10), -random(0, 5));
            this.moves.push(randomForce);
        }
    }

    update() {
        if (!this.dead) {
            this.pos.add(this.vel);
            this.vel.add(this.acc);
            this.acc.set(0,0);
            this.time++;

            if(this.pos.y + this.h < floorY) {
                this.pos.y += .9;
            }
        }
    }
    
    collision(aWall) {
        if(collideRectRect(this.pos.x, this.pos.y, this.w, this.h, aWall.pos.x, aWall.pos.y, aWall.w, aWall.h)) {
            this.acc.set(0,this.acc.y);
            this.vel.set(0,this.vel.y);
            this.dead = true;
        }
    }

    applyForce(aForce) {
        this.acc.add(aForce);
    }

    move() {
        this.vel.mult(0.9);
        if(this.time < this.moves.length && !this.dead && this.time % 2 == 0) {
            this.applyForce(this.moves[this.time]);
        }
    }

    show() {
        fill(125);
        stroke(0);
        rect(this.pos.x, this.pos.y, this.w, this.h);
        this.move();
        this.update();
    }
}