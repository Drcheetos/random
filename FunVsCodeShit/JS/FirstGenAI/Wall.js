class Wall {
    constructor(aX, aY) {
        this.w = 75;
        this.h = random() * 100 + 50;
        this.pos = createVector(aX, aY - this.h);
    }

    show() {
        noStroke();
        fill(255);
        rect(this.pos.x, this.pos.y, this.w, this.h);
    }
}