let radius = 200;
let points = 3;
let pointSlider;

function setup() {
    createCanvas(800, 800);
    pointSlider = createSlider(3, 50, 3, 1);
    pointSlider.position(50,50);
}

function draw() {
    background(0);
    points = pointSlider.value();
    push();
    translate(width/2, height/2 - radius * 0.25);

    stroke(255);
    noFill();
    ellipse(0, 0, radius * 2);

    rotate(-11);
    fill(255);
    beginShape();
    for (let i = 0; i < points; i++) {
      let angle = map(i, 0, points, 0, TWO_PI);
      let x = radius * cos(angle);
      let y = radius * sin(angle);
      vertex(x, y);
    }
    endShape();
    pop();
}