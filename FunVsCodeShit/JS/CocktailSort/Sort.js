class cocktailSort {
    constructor() {
        this.data = [10,5,4,8,7,2,1,6,3,9];
        this.ratio = width / this.data.length;;
        this.goRight = true;
    }

    show() {
        for (let i = 0; i < this.data.length; i++) {
            const current = this.data[i];
            noStroke();
            fill(255);
            rect((i * this.ratio) + 1, height, this.ratio - 2, -current * this.ratio);
        }
    }

    cocktailSort() {
        if(this.goRight) {
            for (let i = 0; i < this.data.length; i++) {
                const current = this.data[i];
                for (let j = 0; j < this.data.length; j++) {
                    const temp = this.data[j];
                    
                    if (i == j) {
                        return;
                    }

                    if (current > temp) {
                        const swap = current;
                        current = temp;
                        temp = swap;

                        if (i == this.data.length - 1) {
                            this.goRight = false;
                        }
                    }
                }
            }
        }
        else {
            for (let i = this.data.length; i > 0; i--) {
                const current = this.data[i];
                for (let j = this.data.length; j > 0; j--) {
                    const temp = this.data[j];
                    
                    if (i == j) {
                        return;
                    }

                    if (current < temp) {
                        const swap = current;
                        current = temp;
                        temp = swap;

                        if (i == 0) {
                            this.goRight = true;
                        }
                    }
                }
            }
        }
    }
}