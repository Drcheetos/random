class Balls {
    constructor(i) {
        this.x = random(0, width);
        this.y = random(0, height);
        this.dia = 25;
        this.velX = random(-5, 5);
        this.velY = random(-10);
        this.time = random(0, 10);
<<<<<<< HEAD
        this.col = color(floor(random(0,255)), floor(random(0,255)), floor(random(0,255)), 200);
=======
        this.col = color(floor(random(0,255)), floor(random(0,255)), floor(random(0,255)), i + random(1,100) % 255);
>>>>>>> 2616ac63d7282254e3f0ccaad9332175a64c48bb
        this.airResist = 0.0025;

        this.hit = false;
    }
<<<<<<< HEAD
=======

    // collide(obj) {
    //     this.hit = collideRectCircle(this.x, this.y, this.w, this.h, obj.x, obj.y, obj.w);

    //     if(this.hit) {
    //         this.velX = 0;
    //         this.velY = 0;
    //         this.col = color(255, 0, 255, 150);
    //     }
    // }
>>>>>>> 2616ac63d7282254e3f0ccaad9332175a64c48bb
    
    move() {
        this.x += this.velX;
        this.y += this.velY;

        if(this.x > width) {
            this.x = 0;
        }

        if(this.x < 0) {
            this.x = width;
        }

        if(this.y > height) {
            this.y = 0;
        }

        if(this.y < 0) {
            this.y = height;
        }

        this.velX -= this.velX * this.airResist;
        this.velY -= this.velY * this.airResist;
    }

    show() {
        fill(this.col);
        noStroke();
        ellipse(this.x, this.y, this.dia);
        this.move();
    }
}