let currentPoint;
let points = [];
let k;
let dia;
let time = 0;
let n;
let nSlider;
let d;
let dSlider;
let temp = 0;

function setup() {
    createCanvas(800, 800);
    background(0);
    dia = width * 0.45;
    currentPoint = createVector();
    nSlider = createSlider(0.000001, 50, 6, 1);
    nSlider.position(65, 65);
    dSlider = createSlider(1, 100, 2, 1);
    dSlider.position(65, 130);
}

function draw() {
    
    if (n != nSlider.value()) {
        n = nSlider.value();
        points = [];
        time = 0;
        background(0);
    }
    
    if (d != dSlider.value()) {
        d = dSlider.value();
        points = [];
        time = 0;
        background(0);
    }

    k = n/d;

    translate(width / 2, height / 2);
    time += 10.23;

    temp += 1;
    if(temp == 120) {
        temp = 0;
    }

    color = map(temp, 0, 360, 155, 255);

    let angle = map(time, 0, 10, 0, TWO_PI);

    currentPoint.x = (cos(k * angle) * cos(angle)) * dia;
    currentPoint.y = (cos(k * angle) * sin(angle)) * dia;

    points.push(currentPoint);

    for(let i = 0; i < points.length; i++) {
        noStroke();
        fill(color * 0.75, random(color), color);
        ellipse(points[i].x, points[i].y, 2);
    }
}