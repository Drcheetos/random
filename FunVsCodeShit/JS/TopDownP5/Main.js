let player;
let enemies = [];
let maxEnemies = 100;
let bullets = [];
let bloodFXs = [];
let drops = [];
let kills = 0;
let score = 0;
let waveTimerValue = 500;
let waveTimer;
let spawnTimerValue = 10;
let spawnTimer = 0;
let floorIMG;
let startGame = false;
let doOnce = true;

function setup() {
    createCanvas(1000, 500);
    loadImage('Images/floor.jpg', ()=> { floorIMG = loadImage('Images/floor.jpg') }, ()=> { floorIMG = null } );
    player = new Player(createVector(width/2, height/2), 20);
    waveTimer = waveTimerValue;
    frameRate(30);
}

function draw() {
    spawn();
    if (floorIMG != null) {
        image(floorIMG, 0, 0, width, height);
    }
    else {
        background(35);
        fill(255);
    }
    for (let drop in drops) {
        drops[drop].update(player);
    }
    for (let blood in bloodFXs) {
        bloodFXs[blood].show();
    }
    strokeWeight(1);
    for (let bullet in bullets) {
        bullets[bullet].show();
        for (let enemy in enemies) {
            enemies[enemy].collision(bullets[bullet]);
        }
    }
    for (let enemy in enemies) {
        enemies[enemy].update(player);
    }
    player.collision(enemies);
    player.update();
    
    fill(255);
    textSize(25);
    text("FPS : " + int(frameRate()), width * 0.02, height * 0.06);
    text("Score : " + score, width * 0.02, height * 0.1);
    
    if (kills == maxEnemies) {
        waveTimer--;
        if (waveTimer <= 0) {
            spawn();
            waveTimer = waveTimerValue;
            drops = [];
            enemies = [];
            kills = 0;
        }
    }
}

function spawn() {
    spawnTimer--;
    if (enemies.length < maxEnemies && spawnTimer <= 0) {
        let temp = int(random(0, 10));
        if (enemies.length + temp > maxEnemies) {
            temp = maxEnemies - enemies.length;
        }
        for (let i = 0; i < temp; i++) {
            let pos = createVector();
            
            // let a = round(random());
            // if (a == 0) {
                pos.y = random(0, height);
                
                // let b = round(random());
                // if (b == 0) {
                    pos.x = 0;
                // }
                // else {
                //     pos.x = width;
                // }
            // }
            // else {
            //     pos.x = random(0, width);
                
            //     let b = round(random());
            //     if (b == 0) {
            //         pos.y = 0;
            //     }
            //     else {
            //         pos.y = height;
            //     }
            // }
            enemies.push(new Enemy(pos));
            spawnTimer = spawnTimerValue;
        }
    }
}