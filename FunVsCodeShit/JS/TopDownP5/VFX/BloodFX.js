class Blood {
    constructor(x, y) {
        this.x = x + random(-15, 15);
        this.y = y + random(-15, 15);
        this.dia = random(15, 30);
        this.spreadSpeed = random(0, 0.25);
        this.stroke = random(1, 3);
        this.lifeTime = 60;
        this.dead = false;
    }

    show() {
        if(!this.dead) {
            this.dia += this.spreadSpeed;
            fill(255, 0, 0);
            noStroke();
            ellipse(this.x, this.y, this.dia);

            if(this.lifeTime <= 0) {
                this.dead = true;
            }
            else {
                this.lifeTime--;
            }
        }
    }
}