class Bullet {
    constructor(aVect, aDir, aDamage) {
        this.pos = aVect;
        this.dir = aDir;
        this.range = 500;
        this.counter = 0;
        this.lifeTime = 1;
        this.damage = aDamage;
        this.dead = false;
    }

    show() {
        if(!this.dead) {
            push();
            stroke(255);
            strokeWeight(1);
            translate(this.pos.x, this.pos.y);
            this.dir.setMag(this.range);
            line(0, 0, this.dir.x, this.dir.y);
            pop();

            if(this.counter >= this.lifeTime) {
                this.dead = true;
            }
            else {
                this.counter++;
            }
        }
    }
}