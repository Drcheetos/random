class Ammo extends Drops {
    pickup(aPlayer) {
        if (aPlayer.totalAmmo == aPlayer.maxTotalAmmo) {
            return false;
        }

        if (aPlayer.totalAmmo + this.amount < aPlayer.maxTotalAmmo) {
            aPlayer.totalAmmo += this.amount;
        }
        else {
            aPlayer.totalAmmo = aPlayer.maxTotalAmmo;
        }
        return true;
    }

    show() {
        fill(0, 0, 255);
        ellipse(this.pos.x, this.pos.y, this.dia);
    }
}