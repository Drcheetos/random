class Drops {
    constructor(aPos, aAmount) {
        this.pos = aPos.copy();
        this.dia = 10;
        this.used = false;
        this.amount = aAmount;
    }
    
    update(aPlayer) {
        this.collision(aPlayer);
        if (!this.used) {
            this.show();
        }
    }

    collision(aPlayer) {
        if (!this.used && collideCircleCircle(this.pos.x, this.pos.y, this.dia, aPlayer.pos.x, aPlayer.pos.y, aPlayer.dia)) {
            if (this.pickup(aPlayer)) {
                this.used = true;
            }
        }
    }

    pickup(aPlayer) {}

    show() {}
}