class Health extends Drops {
    pickup(aPlayer) {
        if (aPlayer.health == aPlayer.maxHealth) {
            return false;
        }

        if (aPlayer.health + this.amount < aPlayer.maxHealth) {
            aPlayer.health += this.amount;
        }
        else {
            aPlayer.health = aPlayer.maxHealth;
        }
        return true;
    }

    show() {
        fill(0, 255, 0);
        ellipse(this.pos.x, this.pos.y, this.dia);
    }
}