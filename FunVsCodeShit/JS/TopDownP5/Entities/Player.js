class Player {
    constructor(aVect, aDia) {
        this.dia = aDia;
        this.pos = aVect;
        this.vel = createVector();
        this.acc = createVector();
        this.forceX = createVector();
        this.forceY = createVector();
        this.dir = createVector(0,1);
        this.controls = [87, 65, 83, 68, 82]; // up, left, down, right, R,  
        this.maxSpeed = 4;
        this.maxSpeedShooting = 0.75;
        this.speed = 3;
        
        this.isHit = false;
        this.hitCooldown = 60;
        this.hitCounter = this.hitCooldown;
        this.maxHealth = 100;
        this.health = this.maxHealth;
        
        this.isShooting = false;
        this.isReloading = false;
        this.reloadTime = 30;
        this.reloadCounter = this.reloadTime;
        this.rateOfFire = 3;
        this.ROFCounter = this.rateOfFire;
        this.maxAmmoInClip = 30;
        this.currentAmmoInClip = 30;
        this.maxTotalAmmo = 120;
        this.totalAmmo = 120;
        this.attackDamage = 5;
    }

    update() {
        this.reload();
        this.shoot();
        this.show();
        this.move();
        this.pos.add(this.vel);
        this.checkPos();
        this.vel.add(this.acc);
        this.capMaxVel();
        this.acc.set(0, 0);
        this.getDamaged();
    }

    getDamaged() {
        if (this.isHit) {
            this.hitCounter--;
            if(this.hitCounter <= 0) {
                this.isHit = false;
                this.hitCounter = this.hitCooldown;
            }
        }
    }

    capMaxVel() {
        if(this.isShooting || this.isReloading || this.isHit) {
            this.vel.limit(this.maxSpeedShooting);
        }
        else {
            this.vel.limit(this.maxSpeed);
        }
    }

    checkPos() {
        if(this.pos.x > width) {
            this.pos.x = 0;
        }
        else if(this.pos.x <= 0) {
            this.pos.x = width;
        }

        if(this.pos.y > height) {
            this.pos.y = 0;
        }
        else if (this.pos.y <= 0) {
            this.pos.y = height;
        }
    }

    applyForce(aForce) {
        this.acc.add(aForce);
    }

    reload() {
        if (keyIsDown(this.controls[4]) && this.currentAmmoInClip < this.maxAmmoInClip && this.totalAmmo != 0 || mouseIsPressed && this.currentAmmoInClip == 0 && !this.isShooting) {
            this.isReloading = true;
            let temp = this.maxAmmoInClip - this.currentAmmoInClip;
            if (temp < this.totalAmmo && this.totalAmmo - temp > 0) {
                this.currentAmmoInClip += temp;
                this.totalAmmo -= temp;
            }
            else {
                this.currentAmmoInClip += this.totalAmmo;
                this.totalAmmo = 0;
            }
        }

        if (this.isReloading) {
            this.reloadCounter--;
            if(this.reloadCounter <= 0) {
                this.isReloading = false;
                this.reloadCounter = this.reloadTime;
            }
        }
    }

    move() {
        if (keyIsDown(this.controls[0])) {
            this.forceY.set(0, -1);
        }
        else if (keyIsDown(this.controls[2])) {
            this.forceY.set(0, 1);
        }
        else {
            this.forceY.set(0,0);
        }
        
        if (keyIsDown(this.controls[1])) {
            this.forceX.set(-1, 0);
        }
        else if (keyIsDown(this.controls[3])) {
            this.forceX.set(1, 0);
        }
        else {
            this.forceX.set(0,0);
        }

        let force = this.forceX.add(this.forceY);
        force.normalize();
        force.mult(this.speed);
        this.applyForce(force);
        this.vel.mult(0.9);
    }

    collision(aEnemies) {
        for (let enemy in aEnemies) {
            if (!aEnemies[enemy].dead && collideCircleCircle(this.pos.x, this.pos.y, this.dia, aEnemies[enemy].pos.x, aEnemies[enemy].pos.y, aEnemies[enemy].dia)) {
                this.isHit = true;
                break;
            }
        }
    }
    
    shoot() {
        this.lookAt(mouseX, mouseY);
        if (mouseIsPressed && !this.isReloading) {
            this.isShooting = true;
            if( this.currentAmmoInClip > 0 ) {
                if (this.ROFCounter <= 0) {
                    bullets.push(new Bullet(this.pos, this.dir, this.attackDamage));
                    this.ROFCounter = this.rateOfFire;
                    this.currentAmmoInClip--;
                }
                else {
                    this.ROFCounter--;
                }
            }
        }
        else {
            this.isShooting = false;
            bullets = [];
        }
    }

    lookAt(aX, aY) { 
        this.dir.x = aX - this.pos.x;
        this.dir.y = aY - this.pos.y;
        this.dir.normalize();
    }

    show() {
        fill(255);
        text("Ammo : " + this.currentAmmoInClip + " / " + this.totalAmmo, width * 0.02, height * 0.96);
        text("Health : " + this.health + " / " + this.maxHealth, width * 0.02, height * 0.92);
        push();
        strokeWeight(10);
        stroke(75);
        translate(this.pos.x, this.pos.y);
        line(0,0,this.dir.x * 20, this.dir.y * 20);
        fill(255);
        noStroke();
        ellipse(0, 0, this.dia);
        pop();
    }
}