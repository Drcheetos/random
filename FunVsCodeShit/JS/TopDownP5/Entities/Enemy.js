class Enemy {
    constructor(aVect) {
        this.pos = aVect;
        this.dir = createVector();
        this.vel = createVector();
        this.acc = createVector();
        this.maxSpeed = 1;
        this.recoil = .25;
        this.dia = 20;
        this.attackSpeed = 60;
        this.attackCounter = 0;
        this.hp = 10;
        this.dead = false;
        this.possibleDrops = ["Health", "Health", "Ammo", "Ammo", "Nothing", "Nothing", "Nothing", "Nothing", "Nothing"];
    }

    collision(aBullet) {
        if(!aBullet.dead) {
            this.hit = collideLineCircle( aBullet.pos.x, aBullet.pos.y, aBullet.pos.x + aBullet.dir.x, aBullet.pos.y + aBullet.dir.y,  this.pos.x, this.pos.y, this.dia);
            if(this.hit) {
                this.hp -= aBullet.damage;
                this.vel.mult(this.recoil);
            }
        }
    }
    
    update(aPlayer) {
        if (!this.dead) {
            this.show();
            this.pos.add(this.vel);
            this.vel.add(this.acc);
            this.vel.limit(this.maxSpeed);
            this.acc.set(0, 0);
            this.move(aPlayer);
            
            if (this.hp <= 0) {
                this.chooseDrop();
                for (let i = 0; i < 5; i++) {
                    bloodFXs.push(new Blood(this.pos.x, this.pos.y));
                }
                kills++;
                score++;
                this.dead = true;
            }
        }
    }
    
    chooseDrop() {
        let rand = random(this.possibleDrops);
        if (rand == "Ammo") {
            drops.push(new Ammo(this.pos, 15));
        }
        else if (rand == "Health") {
            drops.push(new Health(this.pos, 5));
        }
        else {
            console.log("Nothing");
        }

    }

    move(aPlayer) {
        this.attackCounter--;
        if (collideCircleCircle(this.pos.x, this.pos.y, this.dia, aPlayer.pos.x, aPlayer.pos.y, aPlayer.dia)) {
            this.vel.set(0,0);
            if (this.attackCounter <= 0) {
                aPlayer.health--;
                this.attackCounter = this.attackSpeed;
            }
        }
        else {
            this.lookAt(aPlayer.pos.x, aPlayer.pos.y);
            this.applyForce(this.dir.mult(0.2));
        }
    }

    applyForce(aForce) {
        this.acc.add(aForce);
    }

    lookAt(aX, aY) { 
        this.dir.x = aX - this.pos.x;
        this.dir.y = aY - this.pos.y;
        this.dir.normalize();
    }

    show() {
        if(!this.dead) {
            fill(50, 0, 0);
            noStroke();
            ellipse(this.pos.x, this.pos.y, this.dia);
        }
    }
}