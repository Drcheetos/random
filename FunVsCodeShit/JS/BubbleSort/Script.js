let data;
let frames = 0;
let time = 0;
let currentFrameRate = 59;

function setup() {
    createCanvas(1000, 1000);
    data = new Bars();
    frameRate(int(currentFrameRate));
}

function draw() {
    background(0);
    getShowTime();
    data.show();
}

function getShowTime() {
    frames += 1;
    time = frames / currentFrameRate;
    fill(255);
    noStroke();
    textSize(30);
    text("Time since start : " + int(time) + " s", 10, 30);
}