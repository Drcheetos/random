float[] values;

int valueWidth = 0;
int i = 0;
int j = 0;

void setup(){
  size(1500,500);
  valueWidth = floor(width);
  values = new float[valueWidth];
  for (int i = 0; i < values.length; i++) {
    values[i] = random(height);
    }
}

void draw(){
  background(0);
  for (int i = 0; i < values.length; i++) {
    colorMode(HSB,255);
      stroke((i * 10) / 255, (values[i] * 100) / 255, 255);
      line(i, height, i, height - values[i]);
  }

   for (int n = 0; n < 1000; n++) {
       
     float a = values[j];
     float b = values[j+1];
     if ( a > b ) {
        swap(values, j, j+1);
     }
   
     if(i < values.length) {
       j = j + 1;
       if(j >= values.length - i - 1) { 
        j = 0;
        i = i + 1;
       }
     } else {
       println("finished");
       noLoop();
     }
   }
}

void swap(float[] arr, int a, int b) {
  float temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}
